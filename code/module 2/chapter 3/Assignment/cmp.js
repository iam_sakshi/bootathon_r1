function tr() {
    var a = document.getElementById("t1");
    var b = document.getElementById("t2");
    var c = document.getElementById("t3");
    var x = +a.value;
    var y = +b.value;
    var z = +c.value;
    if (x == y && y == z) {
        document.getElementById("l1").innerHTML = "Equilateral triangle";
    }
    else if ((x == y && x != z) || (y == z && y != x) || (x == z && z != y)) {
        document.getElementById("l1").innerHTML = "issoceles triangle";
        if (Math.pow(x, 2) == Math.pow(y, 2) + Math.pow(z, 2) || Math.pow(y, 2) == Math.pow(x, 2) + Math.pow(z, 2) || Math.pow(z, 2) == Math.pow(y, 2) + Math.pow(x, 2)) {
            document.getElementById("l2").innerHTML = "right triangle";
        }
    }
    else {
        document.getElementById("l1").innerHTML = "scalene triangle";
    }
}
//# sourceMappingURL=cmp.js.map