function sin1() {
    let t11 = document.getElementById("t11");
    let t12 = document.getElementById("t12");
    var a = Math.PI / 180 * parseFloat(t11.value);
    var b = Math.sin(a);
    t12.value = b.toString();
}
function cos1() {
    let t21 = document.getElementById("t21");
    let t22 = document.getElementById("t22");
    var a = Math.PI / 180 * parseFloat(t21.value);
    var b = Math.cos(a);
    t22.value = b.toString();
}
//# sourceMappingURL=math1.js.map