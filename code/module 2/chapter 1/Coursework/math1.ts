function sin1()
{
  let t11:HTMLInputElement=<HTMLInputElement>document.getElementById("t11");
  let t12:HTMLInputElement=<HTMLInputElement>document.getElementById("t12");
  var a:number=Math.PI/180*parseFloat(t11.value);
  var b:number=Math.sin(a);
  t12.value=b.toString();
}
function cos1()
{
    let t21:HTMLInputElement=<HTMLInputElement>document.getElementById("t21");
    let t22:HTMLInputElement=<HTMLInputElement>document.getElementById("t22");
    var a:number=Math.PI/180*parseFloat(t21.value);
    var b:number=Math.cos(a);
    t22.value=b.toString();
}