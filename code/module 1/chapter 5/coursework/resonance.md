# RESONANCE

## *AIM*
 
  >  TO SHOW THAT WHEN XL IS EQUAL TO XC RESONANCE IN THE CIRCUIT IS VISIBLE

## *THEORY*

>Thus far we have analysed the behaviour of a series RLC circuit whose source voltage is a fixed frequency steady state sinusoidal supply. We have also seen in our tutorial about series RLC circuits that two or more sinusoidal signals can be combined using phasors providing that they have the same frequency supply.
<br>

![Resonance circuit](rcl.PNG)


## _PROCEDURE_
>1. Before you connect the circuit to the function generator set the frequency to 60 Hz. Then,  using the voltmeter set the generator output to 5 volts (rms).
  2. Using the proto-board and wire leads connect the resistor, capacitor, and inductor along  with the output of the function generator to construct it. Here  we are measuring the peak to peak voltage across the resistor using the oscilloscope. The  three components are connected in series with the function generator acting as the power  supply. Connect the black leads together at the end of the resistor. 
  >3. Record the values of R, L, and C for this circuit in the space provided in the data section.
  4. Use equation 1 to compute the expected resonance frequency and record your result in  data table 1.
  >5. Change the function generator frequency to 50Hz and record the peak to peak voltage  from the oscilloscope in data table 2. Then, adjust the output frequency to 100 Hz and  record the voltage. Adjust the output frequency to 200 Hz and record the voltage.  Continue adjusting the output frequency to each value below the expected resonance  frequency computed in step 4. Record the voltage for each of these values.
  6. Determine an experimental value for resonance frequency by finding the frequency that  produces the largest voltage on the oscilloscope. Record this frequency and voltage.
   
  > 7. Record the voltage for frequency values that are above the resonance frequency  determined in step 6.  </p>
  8. Turn all equipment off and disconnect the circuit.